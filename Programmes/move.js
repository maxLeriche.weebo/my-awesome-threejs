//key detext
function checkkeyUP(e)
{
    //console.log(move);
    if(e.key==="ArrowUp"||e.key==="z"||e.key==="Z")
    {
        move.up =true;
    }
    if(e.key==="ArrowDown"||e.key==="s"||e.key==="S")
    {
        move.down=true;
    }
    if(e.key==="ArrowLeft"||e.key==="q"||e.key==="Q")
    {
        move.left=true;
    }
    if(e.key==="ArrowRight"||e.key==="d"||e.key==="D")
    {
        move.right=true;
    }
    if(e.key===" ")
    {
        move.jump=true;
    }
    if(e.key==="Shift")
    {
        move.shift=true;
    }
    if(e.key==="Control")
    {
        move.ctrl=true;
    }
}
function checkkeyDOWN(e)	
{
    if(e.key==="ArrowUp"||e.key==="z"||e.key==="Z")
    {
        move.up =false;
    }
    if(e.key==="ArrowDown"||e.key==="s"||e.key==="S")
    {
        move.down=false;
    }
    if(e.key==="ArrowLeft"||e.key==="q"||e.key==="Q")
    {
        move.left=false;
    }
    if(e.key==="ArrowRight"||e.key==="d"||e.key==="D")
    {
        move.right=false;
    }
    if(e.key===" ")
    {
        move.jump=false;
    }
    if(e.key==="Shift")
    {
        move.shift=false;
    }
    
    if(e.key==="Control")
    {
        move.ctrl=false;
    }
}

//Mouvement
function movement(){
    var tomove= camera;
    
    if(move.shift)
    {
        moveparam =0.1;
    }
    else
    {
        moveparam =0.02;
    }

        if(move.up)
        {
            tomove.translateZ(-moveparam);
            
        }
        if(move.down)
        {
            tomove.translateZ(moveparam);
        }
        if(move.left)
        {
            tomove.translateX(-moveparam);
        }
        if(move.right)
        {
            tomove.translateX(moveparam);
        }

}
