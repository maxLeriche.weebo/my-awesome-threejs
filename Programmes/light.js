function InitLight()
{
    var ambient = new THREE.AmbientLight(0xffffff ,0.015);
                scene.add( ambient );
                
    {//setup des toche
        flashLight = new THREE.Mesh(new THREE.CylinderGeometry(1, 1, 7, 20), new THREE.MeshPhongMaterial({color:0x000000}));
        flashLight.rotateX(Math.PI/2);
        flashLight.name="lampetorch";
        camera.add(flashLight);
        var Lintensity = 0.5;
        var Ldistance = 150;
        var Lpower = 4000;
        var Langle =0.2;
        
        spotLight = new THREE.SpotLight(0xffffff, Lintensity, Ldistance);
        spotLight.power = Lpower;
        spotLight.angle = Langle;
        spotLight.decay = 2.5;
        spotLight.penumbra = 0.1;
        spotLight.distance = 200;
        spotLight.castShadow = true;
        spotLight.rotateX(Math.PI/2);

        flashLight.add(spotLight);
        flashLight.add(spotLight.target);

        spotLight2 = new THREE.SpotLight(0xCCCCCC, Lintensity, Ldistance);
        spotLight2.power = Lpower/2;
        spotLight2.angle = Langle+0.05;
        spotLight2.decay = 2.5;
        spotLight2.penumbra = 0.1;
        spotLight2.distance = 200;
        spotLight2.castShadow = true;
        spotLight2.rotateX(Math.PI/2);

        flashLight.add(spotLight2);
        flashLight.add(spotLight2.target);

        
        spotLight3 = new THREE.SpotLight(0xFFFFFF, Lintensity, Ldistance);
        spotLight3.power = Lpower;
        spotLight3.angle = Langle+0.1;
        spotLight3.decay = 3;
        spotLight3.penumbra = 0.1;
        spotLight3.distance = 200;
        spotLight3.castShadow = true;
        spotLight3.rotateX(Math.PI/2);

        flashLight.add(spotLight3);
        flashLight.add(spotLight3.target);
    
    }
}